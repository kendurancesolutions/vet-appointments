import {BiCalendarPlus} from "react-icons/all";
import {useState} from "react";

export const AddAppointment = ({appointments, setAppointments}) => {
	const defaultFormData = {
		"ownerName":  "",
		"petName":    "",
		"aptDate":    "",
		"aptTime":   "",
		"aptNotes":   "",
	}
	let [ toggleForm, setToggleForm ] = useState(false)
	let [ formData, setFormData ] = useState(defaultFormData)
	const class_buttonRounding = toggleForm ? 'rounded-t-md' : 'rounded-md'
	return (    <div>
			<button className={`bg-green-400 text-white px-2 py-3 w-full text-left rounded-t-md ${class_buttonRounding}` }
			onClick={() => {setToggleForm(!toggleForm)}}>
				<div><BiCalendarPlus className="inline-block align-text-top" />  Add Appointment</div>
			</button>
			{toggleForm && <AddAppointmentContent appointments={appointments} setAppointments={setAppointments} setToggleForm={setToggleForm}
			                                      formData={formData} setFormData={setFormData} defaultFormData={defaultFormData} />}
		</div>
	)
}

const AddAppointmentContent = ({appointments, setAppointments, formData, setFormData, defaultFormData, setToggleForm}) => {
	const changeFormData= (e, fieldName) => {
		setFormData({...formData, [fieldName]:e.target.value})
	}
	const resetForm= () => {
		setFormData(defaultFormData)
		// console.log(formData)
	}
	const addToFormData= () => {
		let newApt = {
			"id": determineLastId() + 1,
			"petName":    formData.petName,
			"ownerName":  formData.ownerName,
			"aptNotes":   formData.aptNotes,
			"aptDate":  `${formData.aptDate} ${formData.aptTime}`
		}
		setAppointments([...appointments, newApt])
		resetForm()
		setToggleForm(false)
	}
	const determineLastId = () => {
		const lastId = appointments.reduce((max, item) => Number(item.id) > max ? Number(item.id) : max, 0)
		console.log({lastId})
		return lastId
	}
	return (
		<div className="border-r-2 border-b-2 border-l-2 border-light-green-500 rounded-b-md pl-4 pr-4 pb-4">
			<div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start  sm:pt-5">
				<label htmlFor="ownerName" className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
					Owner Name
				</label>
				<div className="mt-1 sm:mt-0 sm:col-span-2">
					<input type="text" name="ownerName" id="ownerName" onChange={(e) => {changeFormData(e,"ownerName")}} value={formData.ownerName}
					       className="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md" />
				</div>
			</div>

			<div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start  sm:pt-5">
				<label htmlFor="petName" className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
					Pet Name
				</label>
				<div className="mt-1 sm:mt-0 sm:col-span-2">
					<input type="text" name="petName" id="petName" onChange={(e) => {changeFormData(e, "petName")}} value={formData.petName}
					       className="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md" />
				</div>
			</div>

			<div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start  sm:pt-5">
				<label htmlFor="aptDate" className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
					Apt Date
				</label>
				<div className="mt-1 sm:mt-0 sm:col-span-2">
					<input type="date" name="aptDate" id="aptDate" onChange={(e) => {changeFormData(e, "aptDate")}} value={formData.aptDate}
					       className="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md" />
				</div>
			</div>

			<div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start  sm:pt-5">
				<label htmlFor="aptTime" className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
					Apt Time
				</label>
				<div className="mt-1 sm:mt-0 sm:col-span-2">
					<input type="time" name="aptTime" id="aptTime" onChange={(e) => {changeFormData(e, "aptTime")}} value={formData.aptTime}
					       className="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md" />
				</div>
			</div>

			<div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start  sm:pt-5">
				<label htmlFor="aptNotes" className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
					Appointment Notes
				</label>
				<div className="mt-1 sm:mt-0 sm:col-span-2">
            <textarea id="aptNotes" name="aptNotes" rows="3" placeholder="Detailed comments about the condition" onChange={(e) => {changeFormData(e, "aptNotes")}}
                      className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md" value={formData.aptNotes}/>
				</div>
			</div>


			<div className="pt-5">
				<div className="flex justify-end">
					<button type="button" onClick={resetForm} className="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-400">
						Reset
					</button>
					<button type="submit" onClick={addToFormData} className="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-400">
						Submit
					</button>
				</div>
			</div>
		</div>
	)
}

