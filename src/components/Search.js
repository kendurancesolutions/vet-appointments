import {BiCaretDown, BiCaretUp, BiCheck, BiSearch} from "react-icons/bi";


export const Search = ({toggleDropdown, setToggleDropdown, query, setQuery,
	                       sortBy, orderBy, setOrderBy, setSortBy}) => {

	return(<div className="mb-5">
		<div className="mt-1 relative rounded-md shadow-sm">
			<div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
				<BiSearch />
				<label htmlFor="query" className="sr-only" />
			</div>
			<input type="text" name="query" id="query" value={query}
			       onChange={e=>{setQuery(e.target.value)}}
			       className="pl-8 rounded-md focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300" placeholder="Search" />
			<div className="absolute inset-y-0 right-0 flex items-center">
				<div>
					<button type="button" onClick={() => setToggleDropdown(!toggleDropdown)}
					        className="justify-center px-4 py-2 bg-green-400 border-2 border-green-400 text-sm text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 flex items-center rounded-r-md" id="options-menu" aria-haspopup="true" aria-expanded="true">
						Sort By {toggleDropdown ? <BiCaretUp className="ml-2" /> : <BiCaretDown className="ml-2" />}
					</button>
					{/*{ toggleDropdown && <Dropdown /> }*/}
					<Dropdown isShowing={toggleDropdown} setOrderBy={setOrderBy} setSortBy={setSortBy}
					          sortBy={sortBy} orderBy={orderBy} />
				</div>
			</div>
		</div>
	</div>)
}

const Dropdown = ({isShowing, sortBy, orderBy, setOrderBy, setSortBy}) => {
	if(!isShowing) return null
	return (    <div className="origin-top-right absolute right-0 mt-2 w-56
      rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
		<div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
			<div onClick={() => {setSortBy("petName")}}
				className="px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 flex justify-between cursor-pointer"
				role="menuitem">Pet Name {dropdown_toggleBiCheck(sortBy,"petName")}</div>
			<div onClick={() => {setSortBy("ownerName")}}
				className="px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 flex justify-between cursor-pointer"
				role="menuitem">Owner Name  {dropdown_toggleBiCheck(sortBy,"ownerName")}</div>
			<div onClick={() => {setSortBy("aptDate")}}
				className="px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 flex justify-between cursor-pointer"
				role="menuitem">Date {dropdown_toggleBiCheck(sortBy,"aptDate")}</div>
			<div onClick={() => {setOrderBy("asc")}}
				className="px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 flex justify-between cursor-pointer border-gray-1 border-t-2"
				role="menuitem">Asc {dropdown_toggleBiCheck(orderBy,"asc")}</div>
			<div onClick={() => {setOrderBy("desc")}}
				className="px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 flex justify-between cursor-pointer"
				role="menuitem">Desc {dropdown_toggleBiCheck(orderBy, "desc")}</div>
		</div>
	</div>)
}

const dropdown_toggleBiCheck = (a, b) => {
	return (a === b ? <BiCheck /> : <></>)
}