import { GiSittingDog } from "react-icons/gi"
import {Search} from "./components/Search";
import {AddAppointment} from "./components/AddAppointment";
import {AppointmentList} from "./components/ListAppointments";
import {useState} from "react";
import {Footer} from "./components/body/Footer";

export default function App() {

  let [appointments, setAppointments]       = useState([])
  let [ toggleDropdown, setToggleDropdown ] = useState(false)
  let [ query, setQuery ]     = useState("")
  let [ sortBy, setSortBy ]   = useState("petName")
  let [ orderBy, setOrderBy ] = useState("asc")

  const filteredAppointments = appointments.filter((apt) => {
    return(
      apt_comparer(apt.petName, query)    ||
      apt_comparer(apt.ownerName, query)  ||
      apt_comparer(apt.aptNotes, query)
    )
  }).sort((a,b) => apt_sorter(a,b,orderBy,sortBy))

  return (
    <div className="App container mx-auto mt-3 font-thin">
      <h1 className="text-5xl pb-2">
        <GiSittingDog className="inline-block text-gray-700 align-top"/> Vet Appointments
      </h1>
      <AddAppointment appointments={appointments} setAppointments={setAppointments}/>
      <Search toggleDropdown={toggleDropdown} setToggleDropdown={setToggleDropdown}
              query={query} setQuery={setQuery} sortBy={sortBy} orderBy={orderBy}
              setOrderBy={setOrderBy} setSortBy={setSortBy} />
      <AppointmentList appointments={appointments} setAppointments={setAppointments} filteredAppointments={filteredAppointments} />
      <Footer />
    </div>
  );
}

const apt_comparer = (str, query) => {
  return str.toLowerCase().includes(query.toLowerCase())
}
const apt_sorter = (a,b, orderBy, sortBy) => {
  let order = (orderBy === 'asc') ? 1 : -1
  const comparer = a[sortBy].toLowerCase() < b[sortBy].toLowerCase()
  return( comparer ? (-1 * order) : (1 * order) )
}

