import {useEffect, useCallback} from "react";
import {BiTrash} from "react-icons/bi";

export const AppointmentList = ({appointments, setAppointments, filteredAppointments}) => {
	const url = '/VetAppointments/data/Appointments.json'

	const fetchData = useCallback(() => {
		fetch(url)
			// .then(response => console.log({response}))
			.then(response => response.json())
			.then(data => setAppointments(data))
	}, [])

	useEffect(() => {
		fetchData()
	}, [fetchData])

	const removeAppointment = (_apt_id) => {
		console.log(`removing ${_apt_id}`)
		setAppointments(appointments.filter(apt=>apt.id.toString() !==_apt_id.toString()))
	}

	return (
		<ul className="divide-y divide-gray-200">
			{ filteredAppointments.map( apt => (
					<AppointmentListItem apt={apt} key={apt.id} onRemoveAppointment={()=>removeAppointment(apt.id)} />
				) ) }
		</ul>

	)
}




const AppointmentListItem = ({apt, onRemoveAppointment}) => {
	// apt = apt.apt
	return (
		<li className="px-3 py-3 flex items-start" key={apt.id} id={`apt_${apt.id}`}>
			<button type="button" onClick={onRemoveAppointment} className="p-1.5 mr-1.5 mt-1 rounded text-white bg-red-400 hover:bg-yellow-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">
			<BiTrash /></button>
			<div className="flex-grow">
				<div className="flex items-center">
					<span className="flex-none font-medium text-2xl text-green-500">{apt.petName}</span>
					<span className="flex-grow text-right">{apt.aptDate}</span>
				</div>
				<div><b className="font-bold text-green-500">Owner:</b> {apt.ownerName}</div>
				<div className="leading-tight">{apt.aptNotes}</div>
			</div>
		</li>
	)
}