

export const Footer = () => {
	return(
		<footer className="absolute left-0 mt-3 right-0">
			<p className="container mx-auto text-right bg-green-400 text-white px-2 py-3 rounded-t-md font-normal">
				&copy; Kendurance Solutions
			</p>
		</footer>
	)
}